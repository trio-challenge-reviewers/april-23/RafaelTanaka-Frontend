import { module, test } from 'qunit';
import { setupRenderingTest } from 'wordle-challenge/tests/helpers';
import { render, triggerKeyEvent, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | match', function (hooks) {
  setupRenderingTest(hooks);

  test('it test the component validations', async function (assert) {
    // It loads mock words in ember-data
    let store = this.owner.lookup('service:store');
    store.unloadAll('wordle-word');
    store.pushPayload({
      'wordle-words': [{ word: 'FIRST' }, { word: 'THIRD' }, { word: 'OTHER' }],
    });

    await waitUntil(
      function () {
        return store.peekAll('wordle-word').length === 3;
      },
      { timeout: 5000 }
    );

    // Get the list of available words and one word to guess.
    let wordToGuess = store.peekRecord('wordle-word', 'OTHER');
    let availableWords = store.peekAll('wordle-word');

    // Set the values to a fictional controller
    this.set('wordToGuess', wordToGuess);
    this.set('availableWords', availableWords);

    // It renders the component in the test engine
    await render(hbs`<Match 
          @wordleWord={{this.wordToGuess}} 
          @validWords={{this.availableWords}}
          />`);

    await triggerKeyEvent('.match--container', 'keydown', 'Enter');
    await waitUntil(
      function () {
        return document.querySelectorAll('.toastify').length;
      },
      { timeout: 5000 }
    );
    assert
      .dom(document.querySelector('.toastify'))
      .hasText(
        'Not enough letters',
        'It validates when user tries to press enter without completing the guess'
      );

    await waitUntil(
      function () {
        return document.querySelectorAll('.toastify').length === 0;
      },
      { timeout: 5000 }
    );

    await typeWordAndConfirm('apple');
    await waitUntil(
      function () {
        return document.querySelectorAll('.toastify').length === 1;
      },
      { timeout: 5000 }
    );
    assert
      .dom(document.querySelector('.toastify'))
      .hasText(
        'Not in word list',
        'It validates when user tries to guess a word that is not valid (not contained in word list)'
      );
    await waitUntil(
      function () {
        return document.querySelectorAll('.toastify').length === 0;
      },
      { timeout: 5000 }
    );
  });

  test('it tests the color indicators', async function (assert) {
    // It loads mock words in ember-data
    let store = this.owner.lookup('service:store');
    store.unloadAll('wordle-word');
    store.pushPayload({
      'wordle-words': [
        { word: 'GOOFY' },
        { word: 'THIEF' },
        { word: 'HYDRO' },
        { word: 'THROW' },
        { word: 'OTTER' },
        { word: 'OTHER' },
      ],
    });

    await waitUntil(
      function () {
        return store.peekAll('wordle-word').length === 6;
      },
      { timeout: 5000 }
    );

    // Get the list of available words and one word to guess.
    let wordToGuess = store.peekRecord('wordle-word', 'OTHER');
    let availableWords = store.peekAll('wordle-word');
    let scenarios = [
      {
        word: 'goofy',
        expectedIndicators: ['gray', 'yellow', 'gray', 'gray', 'gray'],
      },
      {
        word: 'thief',
        expectedIndicators: ['yellow', 'yellow', 'gray', 'green', 'gray'],
      },
      {
        word: 'hydro',
        expectedIndicators: ['yellow', 'gray', 'gray', 'yellow', 'yellow'],
      },
      {
        word: 'throw',
        expectedIndicators: ['yellow', 'yellow', 'yellow', 'yellow', 'gray'],
      },
      {
        word: 'otter',
        expectedIndicators: ['green', 'green', 'gray', 'green', 'green'],
      },
      {
        word: 'other',
        expectedIndicators: ['green', 'green', 'green', 'green', 'green'],
        successScenario: true,
      },
    ];

    // Set the values to a fictional controller
    this.set('wordToGuess', wordToGuess);
    this.set('availableWords', availableWords);

    // It renders the component in the test engine
    await render(hbs`<Match 
          @wordleWord={{this.wordToGuess}} 
          @validWords={{this.availableWords}}
          />`);

    let currentAttemptIndex = 0;

    // For each scenario (word), type for every attempt and check for each letter the color indicator.
    for (let scenario of scenarios) {
      // Type the word
      await typeWordAndConfirm(scenario.word);

      let currentLetterIndex = 0;

      // Check if the expected colocar indicator exists for every letter.
      for (let indicatorClass of scenario.expectedIndicators) {
        const selector = `#attempt-${
          currentAttemptIndex + 1
        }-letter-${currentLetterIndex}`;
        assert
          .dom(selector)
          .hasText(scenario.word[currentLetterIndex].toUpperCase());
        assert.dom(selector).hasClass(indicatorClass);
        currentLetterIndex++;
      }

      if (scenario.successScenario) {
        await waitUntil(
          function () {
            return document.querySelectorAll('.toastify').length > 1;
          },
          { timeout: 5000 }
        );
        assert.dom(document.querySelector('.toastify')).hasText('Genius');
        await waitUntil(
          function () {
            return document.querySelectorAll('.toastify').length === 0;
          },
          { timeout: 5000 }
        );
      }

      currentAttemptIndex++;
    }
  });
});

async function typeWordAndConfirm(word) {
  for (let letter of word.toUpperCase().split('')) {
    await triggerKeyEvent(
      '.attempt--container',
      'keydown',
      letter.charCodeAt(letter)
    );
  }
  await triggerKeyEvent('.attempt--container', 'keydown', 'Enter');
  return Promise.resolve('ok');
}
