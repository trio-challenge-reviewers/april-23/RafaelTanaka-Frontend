import { module, test } from 'qunit';

import { setupTest } from 'wordle-challenge/tests/helpers';

module('Unit | Adapter | wordle word', function (hooks) {
  setupTest(hooks);

  test('it tests the path name customization', function (assert) {
    let adapter = this.owner.lookup('adapter:wordle-word');
    assert.deepEqual(adapter.pathForType('wordle-word'), 'wordle-words');
  });

  test('it tests the response handling', function (assert) {
    let adapter = this.owner.lookup('adapter:wordle-word');
    let handledResponse = adapter.handleResponse(
      200,
      {},
      ['FIRST', 'SECOND'],
      {}
    );
    assert.deepEqual(handledResponse, {
      wordleWord: [
        {
          word: 'FIRST',
        },
        {
          word: 'SECOND',
        },
      ],
    });
  });
});
