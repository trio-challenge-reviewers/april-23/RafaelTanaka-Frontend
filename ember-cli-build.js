'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function (defaults) {
  const app = new EmberApp(defaults, {
    // Add options here
  });

  // Importing keyboard component...
  app.import('node_modules/simple-keyboard/build/css/index.css');
  app.import('node_modules/simple-keyboard/build/index.js');

  // Importing toastify.css
  app.import('node_modules/toastify-js/src/toastify.css');

  // Importing animate.css
  app.import('node_modules/animate.css/animate.min.css');

  return app.toTree();
};
