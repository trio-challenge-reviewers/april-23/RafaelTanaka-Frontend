import Route from '@ember/routing/route';
import { service } from '@ember/service';
import { hash } from 'rsvp';

export default class IndexRoute extends Route {
  @service wordRandomizer;
  @service store;

  async model() {
    let wordToGuess = await this.wordRandomizer.randomize();
    let availableWords = this.store.peekAll('wordle-word');
    return hash({ wordToGuess, availableWords });
  }
}
