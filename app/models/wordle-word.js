import Model, { attr } from '@ember-data/model';

export default class WordleWordModel extends Model {
  @attr('string') word;
}
