import Service, { service } from '@ember/service';

/*
  I'm defining this separated service to separate which belongs to 
  API request and which belongs to application logic.
  As requirement we need to get one of the values provided by the API 
  and randomize. This service will take care of doing the randomization.
*/

export default class WordRandomizerService extends Service {
  @service store;

  async randomize() {
    let words;

    // Try to get the words from the API
    try {
      words = await this.store.findAll('wordle-word');
    } catch (e) {
      return Promise.reject(e);
    }

    // If there is no words, raise a rejection.
    if (!(words && words.length)) {
      return Promise.reject('There is no words provided by the API');
    }

    return Promise.resolve(words[this.randomizeWordsFromApiResponse(words)]);
  }

  randomizeWordsFromApiResponse(words) {
    let minIndex = Math.ceil(0);
    let maxIndex = Math.floor(words.length - 1);
    return Math.floor(Math.random() * (maxIndex - minIndex + 1) + minIndex);
  }
}
