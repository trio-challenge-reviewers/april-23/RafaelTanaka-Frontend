import Service from '@ember/service';
import Toastify from 'toastify-js';

export default class NotificationService extends Service {
  notify(text) {
    Toastify({
      text: text,
      duration: 3000,
      gravity: 'top',
      position: 'center',
      stopOnFocus: true,
      style: {
        background: 'rgb(86, 86, 86)',
        'border-color': '#a3a3a3',
        'border-style': 'solid',
      },
      offset: {
        y: 100,
      },
    }).showToast();
  }
}
