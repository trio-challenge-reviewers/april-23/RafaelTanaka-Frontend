/*
  As the API is not in JSON API format (https://jsonapi.org) here 
  I'm changing the default application adapter and serializer to REST, so we
  have more flexibility to handle custom responses.
  See:
    https://api.emberjs.com/ember-data/release/classes/RESTAdapter
    https://api.emberjs.com/ember-data/release/classes/RESTSerializer
*/
import RESTSerializer from '@ember-data/serializer/rest';

export default class ApplicationSerializer extends RESTSerializer {}
