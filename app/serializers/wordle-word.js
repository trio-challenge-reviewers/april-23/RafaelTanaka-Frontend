import ApplicationSerializer from './application';

export default class WordleWordSerializer extends ApplicationSerializer {
  /*
    The response contains only a array of values (array of words) without any other attribute or primary key.
    In Ember perspective all the models instance must have a identifier, so here I'm defining the word itself as the primary key.
    See:
      https://api.emberjs.com/ember-data/4.12/classes/RESTSerializer/properties/primaryKey?anchor=primaryKey
  */
  primaryKey = 'word';
}
