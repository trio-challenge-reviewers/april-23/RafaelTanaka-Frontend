import ApplicationAdapter from './application';

export default class WordleWordAdapter extends ApplicationAdapter {
  /*
    Here we're handling the response in order to make every word become a 
    model in ember-data perspective.
    See:
      https://api.emberjs.com/ember-data/4.12/classes/RESTAdapter/methods/handleResponse?anchor=handleResponse
  */
  handleResponse(status, headers, payload, requestData) {
    if (status != 200 || !payload.length) {
      return super.handleResponse(status, headers, payload, requestData);
    }
    let emberDataPayload = [];
    payload.forEach((p) => emberDataPayload.push({ word: p }));
    return { wordleWord: emberDataPayload };
  }
}
