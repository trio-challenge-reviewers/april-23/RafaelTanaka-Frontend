/*
  As the API is not in JSON API format (https://jsonapi.org) here 
  I'm changing the default application adapter and serializer to REST, so we
  have more flexibility to handle custom responses.
  See:
    https://api.emberjs.com/ember-data/release/classes/RESTAdapter
    https://api.emberjs.com/ember-data/release/classes/RESTSerializer
*/
import RESTAdapter from '@ember-data/adapter/rest';
import { pluralize } from 'ember-inflector';

export default class ApplicationAdapter extends RESTAdapter {
  host = 'https://challenge.trio.dev/api';
  namespace = 'v1';

  /*
    Why overwriting this method?
    R: the framework as default would pluralize the endpoint resource name in camelCase style
    like wordleWord.
    Here we're using the ember-inflector to make sure that requests related to wordle-word
    use /wordle-words endpoint instead of /wordleWord.
    See: 
      https://api.emberjs.com/ember-data/4.12/classes/RESTAdapter/methods/pathForType?anchor=pathForType
      https://github.com/emberjs/ember-inflector
  */
  pathForType(modelName) {
    return pluralize(modelName);
  }
}
