import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';

export default class MatchComponent extends Component {
  @service notification;

  attempts = 6;

  @tracked
  currentAttemptNumber = 1;

  @tracked
  guessesByAttempt = [];

  @tracked
  success = false;

  get failed() {
    return Boolean(
      this.currentAttemptGuesses === this.attempts &&
        !this.success &&
        !this.matchActive
    );
  }

  get matchActive() {
    return Boolean(this.currentAttemptNumber <= this.attempts && !this.success);
  }

  get wordleWord() {
    return this.args.wordleWord;
  }

  get validWords() {
    return this.args.validWords;
  }

  get arrayOfAttempts() {
    let array = [];
    for (let i = 1; i <= 6; i++) {
      array.push(i);
    }
    return array;
  }

  get currentAttemptGuesses() {
    return this.guessesByAttempt.filter(
      (guessByAttempt) =>
        guessByAttempt.attemptNumber === this.currentAttemptNumber
    );
  }

  get currentAttemptWord() {
    let word = '';
    let guesses = this.currentAttemptGuesses.sort((a, b) => {
      if (a.letterIndex < b.letterIndex) {
        return -1;
      }
      if (a.letterIndex > b.letterIndex) {
        return 1;
      }
      return 0;
    });
    guesses.forEach((guess) => (word = `${word}${guess.guessingLetter}`));
    return word;
  }

  get isCurrentAttemptReadyToConfirm() {
    let currentGuesses = this.currentAttemptGuesses;
    let ready = true;
    if (currentGuesses.length < 5) return false;
    currentGuesses.every((guess) => {
      if (!guess.guessingLetter.trim().length) {
        ready = false;
        return ready;
      }
      return true;
    });
    return ready;
  }

  isWordValid() {
    return Boolean(
      this.validWords.find(
        (wordModel) =>
          this.currentAttemptWord.toUpperCase() === wordModel.word.toUpperCase()
      )
    );
  }

  completeAttempt() {
    if (!this.isCurrentAttemptReadyToConfirm) {
      this.notification.notify('Not enough letters');
      return;
    }
    if (!this.isWordValid()) {
      this.notification.notify('Not in word list');
      return;
    }
    if (this.currentAttemptWord.toUpperCase() === this.wordleWord.word) {
      this.success = true;
      this.notification.notify('Genius');
    }
    // Increments the counter of attempts - the new attempt will be started
    this.currentAttemptNumber++;

    if (this.currentAttemptNumber > this.attempts) {
      if (!this.success) {
        this.notification.notify(this.wordleWord.word);
      }
      document.addEventListener('keydown', (e) => {
        if (e.key === 'Enter') location.reload();
        e.preventDefault();
      });
    }
  }

  @action
  tryToGuess(e) {
    e.preventDefault();
    this.completeAttempt();
  }

  @action
  onAttemptCompleted() {
    this.completeAttempt();
  }

  @action
  registerAttemptGuessing(attemptNumber, letterIndex, guessingLetter) {
    let guessesByAttempt = this.guessesByAttempt;

    // Check for the current attempt if the guess already exists for the letter.
    let indexFound = -1;
    let currentGuessForIndexInAttempt = guessesByAttempt.find(
      (guessing, index) => {
        if (
          guessing.attemptNumber === attemptNumber &&
          guessing.letterIndex === letterIndex
        ) {
          indexFound = index;
          return true;
        }
      }
    );

    // If the guess already exists, using the index found update the guessing letter in the final array.
    if (indexFound > -1) {
      currentGuessForIndexInAttempt.guessingLetter = guessingLetter;
      guessesByAttempt.splice(indexFound, 1, currentGuessForIndexInAttempt);
    } else {
      // If the guess does not exists, create a new record in the array.
      guessesByAttempt.push({ attemptNumber, letterIndex, guessingLetter });
    }

    // Update the final array.
    this.guessesByAttempt = guessesByAttempt;
  }
}
