import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class MatchAttemptComponent extends Component {
  @tracked
  guessingArray = [];

  get enabled() {
    let enabled = Boolean(
      this.args.number === this.args.currentAttempt && this.args.active
    );

    return enabled;
  }

  get disabled() {
    return !this.enabled;
  }

  get wordleWord() {
    return this.args.wordleWord;
  }

  get arrayOfLetters() {
    let array = [];
    let guessedWord = this.guessedWord;
    for (let i = 0; i < 5; i++) {
      if (guessedWord || guessedWord[i]) {
        array.push(guessedWord[i]);
      } else array.push('');
    }
    return array;
  }

  get reveal() {
    return Boolean(
      this.args.number < this.args.currentAttempt ||
        (this.args.number <= this.args.currentAttempt && !this.args.active)
    );
  }

  get guessedWord() {
    let word = '';
    let guesses = this.guessingArray.sort((a, b) => {
      if (a.index < b.index) {
        return -1;
      }
      if (a.index > b.index) {
        return 1;
      }
      return 0;
    });
    guesses.forEach((guess) => (word = `${word}${guess.letter}`));
    return word.toUpperCase();
  }

  @action
  registerGuessing(index, letter) {
    let guessingArray = this.guessingArray;
    let indexFound = -1;
    let guessing = guessingArray.find((guessing, iIndex) => {
      if (guessing.index === index) {
        indexFound = iIndex;
        return true;
      }
    });

    if (indexFound > -1) {
      guessing.letter = letter;
      guessingArray.splice(indexFound, 1, guessing);
    } else {
      guessingArray.push({ index, letter });
    }
    this.guessingArray = guessingArray;
    this.args.onChangeLetterGuess(this.args.number, index, letter);
  }

  @action
  keyboardListener() {
    const keydownListener = (e) => {
      if (this.enabled) {
        this.handleKeyboardKeyDown(e.key);
        e.stopPropagation();
      }
    };
    if (this.enabled) {
      document.addEventListener('keydown', keydownListener);
    } else {
      document.removeEventListener('keydown', keydownListener);
    }
  }

  handleKeyboardKeyDown(key) {
    key = key.toLowerCase();
    if (key.length === 1 && key >= 'a' && key <= 'z') {
      if (this.guessedWord.trim().length === 5 || key.length > 1) {
        return;
      }
      this.registerGuessing(this.guessedWord.length, key);
    } else if (key === 'backspace' && this.guessedWord.trim().length - 1 >= 0) {
      this.registerGuessing(this.guessedWord.trim().length - 1, '');
    } else if (key === 'enter') {
      this.args.onComplete();
    }
  }
}
