import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class MatchLetterComponent extends Component {
  get letterGuessed() {
    return this.args.value;
  }

  get disabled() {
    return this.args.disabled;
  }
  get letterIndex() {
    return this.args.letterIndex;
  }

  get correctLetter() {
    return this.correctWord[this.letterIndex];
  }

  get correctWord() {
    return this.args.wordleWord.word.toUpperCase();
  }

  get empty() {
    return Boolean(!this.letterGuessed || !this.letterGuessed.trim().length);
  }

  get correct() {
    if (this.empty) return false;
    return Boolean(this.letterGuessed.toUpperCase() === this.correctLetter);
  }

  get wordWithUpperCase() {
    return this.args.wordleWord.word.toUpperCase();
  }

  get guessedWord() {
    return this.args.guessedWord;
  }

  get partiallyCorrect() {
    if (this.empty || this.correct) return false;

    // If the word includes the guessed letter:
    if (this.wordWithUpperCase.includes(this.letterGuessed.toUpperCase())) {
      let word = this.wordWithUpperCase.split('');
      let guessedWord = this.guessedWord.toUpperCase().split('');
      let wordWOCorrects = [];
      let guessedWordWOCorrects = [];
      let correctGuessingsCounter = 0;

      // Remove the correct guessings:
      word.forEach((letter, index) => {
        if (guessedWord[index] && guessedWord[index] === letter) {
          if (index < this.letterIndex) {
            correctGuessingsCounter++;
          }
          return;
        }
        wordWOCorrects.push(letter);
        if (guessedWord[index]) {
          guessedWordWOCorrects.push(guessedWord[index]);
        }
      });

      word = wordWOCorrects.join('');
      guessedWord = guessedWordWOCorrects.join('');

      // Get how many times this letter occurs in the word.
      let letterOccurrences =
        word.split(this.letterGuessed.toUpperCase()).length - 1;

      // Get how many times this letter occurs in the guessed word.
      let letterOccurrencesInAttempt =
        guessedWord
          .substring(0, this.letterIndex + 1 - correctGuessingsCounter)
          .split(this.letterGuessed.toUpperCase()).length - 1;
      if (
        letterOccurrencesInAttempt &&
        letterOccurrencesInAttempt <= letterOccurrences
      )
        return true;
    }
    return false;
  }

  get reveal() {
    return Boolean(this.args.reveal);
  }

  get cssClass() {
    if (this.correct) return 'green animate__animated animate__bounceIn';
    if (this.partiallyCorrect)
      return 'yellow animate__animated animate__bounceIn';
    return 'gray';
  }

  @action
  onChangeGuess() {
    this.args.onChangeGuess(this.letterIndex, this.letterGuessed);
  }
}
