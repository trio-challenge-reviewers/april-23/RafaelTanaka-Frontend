import Component from '@glimmer/component';
import { action } from '@ember/object';
import Keyboard from 'simple-keyboard';

export default class OnScreenKeyboardComponent extends Component {
  @action
  didInsert() {
    new Keyboard({
      onKeyPress: (button) => onKeyPress(button),
      mergeDisplay: true,
      layout: {
        default: [
          'q w e r t y u i o p',
          'a s d f g h j k l',
          'Enter z x c v b n m {backspace}',
        ],
      },
      display: { Enter: 'enter', '{backspace}': '⌫' },
    });

    function onKeyPress(button) {
      switch (button) {
        case '{backspace}':
          button = 'Backspace';
          break;
        case '{enter}':
          button = 'Enter';
          break;
      }
      document.dispatchEvent(new KeyboardEvent('keydown', { key: button }));
    }
  }
}
